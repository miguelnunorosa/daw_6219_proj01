-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: projecto
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
INSERT INTO `alunos` VALUES (1,25,1,'Joao Rodrigues','Rua Teste 1','7800-001','Beja','910 000 000','test1@ipbeja.pt','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,25,1,'Nuno Batista','Rua Teste 2','7800-002','Beja','930 000 000','test2@ipbeja.pt','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,25,1,'Miguel Rosa','Rua Teste 3','7800-003','Beja','960 000 000','miguel.rosa17@gmail.com','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `avaliacoes`
--

LOCK TABLES `avaliacoes` WRITE;
/*!40000 ALTER TABLE `avaliacoes` DISABLE KEYS */;
INSERT INTO `avaliacoes` VALUES (1,4,25,1,6,1,1,12.00,'sim','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,4,25,2,15,2,2,12.00,'sim','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,4,25,3,22,3,3,15.00,'sim','2020-12-09 15:49:08','2020-12-09 15:49:08'),(4,4,25,3,11,3,1,10.00,'sim','2020-12-09 16:05:22','2020-12-09 16:05:22'),(5,4,25,3,1,3,1,12.00,'sim','2020-12-09 16:07:08','2020-12-09 16:07:08'),(6,4,25,3,2,3,2,20.00,'sim','2020-12-09 16:08:27','2020-12-09 16:08:27'),(7,4,25,3,8,3,3,14.00,'sim','2020-12-09 16:09:49','2020-12-09 16:09:49'),(8,4,25,3,6,3,1,12.00,'sim','2020-12-09 16:11:08','2020-12-09 16:11:08'),(9,4,25,3,7,3,2,10.00,'sim','2020-12-09 16:13:13','2020-12-09 16:13:13'),(10,4,25,3,3,3,1,20.00,'sim','2020-12-09 16:14:50','2020-12-09 16:14:50');
/*!40000 ALTER TABLE `avaliacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,1,'CTSP Agropecuária Mediterrânica','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,1,'CTSP Análises Laboratoriais','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,2,'CTSP Apoio à Infância','2020-12-09 15:49:08','2020-12-09 15:49:08'),(4,3,'CTSP Apoio em Cuidados Continuados Integrados','2020-12-09 15:49:08','2020-12-09 15:49:08'),(5,4,'CTSP Comércio Internacional','2020-12-09 15:49:08','2020-12-09 15:49:08'),(6,1,'CTSP Culturas Regadas','2020-12-09 15:49:08','2020-12-09 15:49:08'),(7,2,'CTSP Desporto, Lazer e Bem-Estar','2020-12-09 15:49:08','2020-12-09 15:49:08'),(8,4,'CTSP Eletrónica e Computadores','2020-12-09 15:49:08','2020-12-09 15:49:08'),(9,4,'CTSP Gestão de Organizações Sociais','2020-12-09 15:49:08','2020-12-09 15:49:08'),(10,4,'CTSP Informação e Comercialização Turística','2020-12-09 15:49:08','2020-12-09 15:49:08'),(11,1,'CTSP Inovação e Tecnologia Alimentar','2020-12-09 15:49:08','2020-12-09 15:49:08'),(12,2,'CTSP Psicogerontologia','2020-12-09 15:49:08','2020-12-09 15:49:08'),(13,4,'CTSP Redes e Sistemas Informáticos','2020-12-09 15:49:08','2020-12-09 15:49:08'),(14,1,'CTSP Sistemas de Proteção do Ambiente','2020-12-09 15:49:08','2020-12-09 15:49:08'),(15,2,'CTSP Som e Imagem','2020-12-09 15:49:08','2020-12-09 15:49:08'),(16,4,'CTSP Tecnologias Web e Dispositivos Móveis','2020-12-09 15:49:08','2020-12-09 15:49:08'),(17,1,'CTSP Viticultura e Enologia','2020-12-09 15:49:08','2020-12-09 15:49:08'),(18,1,'Licenciatura Agronomia','2020-12-09 15:49:08','2020-12-09 15:49:08'),(19,2,'Licenciatura Audiovisual e Multimédia','2020-12-09 15:49:08','2020-12-09 15:49:08'),(20,1,'Licenciatura Ciência e Tecnologia dos Alimentos','2020-12-09 15:49:08','2020-12-09 15:49:08'),(21,2,'Licenciatura Desporto','2020-12-09 15:49:08','2020-12-09 15:49:08'),(22,2,'Licenciatura Educação Básica','2020-12-09 15:49:08','2020-12-09 15:49:08'),(23,3,'Licenciatura Enfermagem','2020-12-09 15:49:08','2020-12-09 15:49:08'),(24,1,'Licenciatura Engenharia do Ambiente','2020-12-09 15:49:08','2020-12-09 15:49:08'),(25,4,'Licenciatura Engenharia Informática','2020-12-09 15:49:08','2020-12-09 15:49:08'),(26,4,'Licenciatura Gestão de Empresas','2020-12-09 15:49:08','2020-12-09 15:49:08'),(27,4,'Licenciatura Gestão de Empresas (Pós-Laboral)','2020-12-09 15:49:08','2020-12-09 15:49:08'),(28,2,'Licenciatura Serviço Social','2020-12-09 15:49:08','2020-12-09 15:49:08'),(29,4,'Licenciatura Solicitadoria','2020-12-09 15:49:08','2020-12-09 15:49:08'),(30,4,'Licenciatura Solicitadoria (Ensino à Distância)','2020-12-09 15:49:08','2020-12-09 15:49:08'),(31,1,'Licenciatura Tecnologias Bioanalíticas','2020-12-09 15:49:08','2020-12-09 15:49:08'),(32,2,'Licenciatura Terapia Ocupacional','2020-12-09 15:49:08','2020-12-09 15:49:08'),(33,4,'Licenciatura Turismo','2020-12-09 15:49:08','2020-12-09 15:49:08'),(34,1,'Mestrado Agronomia','2020-12-09 15:49:08','2020-12-09 15:49:08'),(35,2,'Mestrado Atividade Física e Saúde','2020-12-09 15:49:08','2020-12-09 15:49:08'),(36,4,'Mestrado Contabilidade e Finanças','2020-12-09 15:49:08','2020-12-09 15:49:08'),(37,2,'Mestrado Desenvolvimento Comunitário e Empreendedorismo','2020-12-09 15:49:08','2020-12-09 15:49:08'),(38,2,'Mestrado Educação Especial - Especialização no Domínio Cognitivo e Motor','2020-12-09 15:49:08','2020-12-09 15:49:08'),(39,4,'Mestrado Engenharia de Segurança Informática','2020-12-09 15:49:08','2020-12-09 15:49:08'),(40,1,'Mestrado Engenharia do Ambiente','2020-12-09 15:49:08','2020-12-09 15:49:08'),(41,3,'Mestrado Estudos em Enfermagem','2020-12-09 15:49:08','2020-12-09 15:49:08'),(42,2,'Mestrado Gerontologia Social e Comunitária','2020-12-09 15:49:08','2020-12-09 15:49:08'),(43,4,'Mestrado Internet das Coisas','2020-12-09 15:49:08','2020-12-09 15:49:08'),(44,4,'Mestrado Segurança e Higiene no Trabalho','2020-12-09 15:49:08','2020-12-09 15:49:08'),(45,2,'Mestrado Serviço Social - Riscos Sociais e Desenvolvimento Local','2020-12-09 15:49:08','2020-12-09 15:49:08'),(46,1,'Pós-Graduação Sistemas de Informação Geográfica no Setor Florestal','2020-12-09 15:49:08','2020-12-09 15:49:08'),(47,4,'Pós-Graduação Turismo - Gestão e Inovação','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `disciplinas`
--

LOCK TABLES `disciplinas` WRITE;
/*!40000 ALTER TABLE `disciplinas` DISABLE KEYS */;
INSERT INTO `disciplinas` VALUES (1,'Álgebra Linear','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,'Análise Matemática','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,'Arquitectura de Computadores','2020-12-09 15:49:08','2020-12-09 15:49:08'),(4,'Bases de Dados I','2020-12-09 15:49:08','2020-12-09 15:49:08'),(5,'Física Aplicada à Computação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(6,'Introdução à Programação I','2020-12-09 15:49:08','2020-12-09 15:49:08'),(7,'Introdução à Programação II','2020-12-09 15:49:08','2020-12-09 15:49:08'),(8,'Probabilidades e Estatística','2020-12-09 15:49:08','2020-12-09 15:49:08'),(9,'Programação Orientada por Objectos','2020-12-09 15:49:08','2020-12-09 15:49:08'),(10,'Sistemas Digitais','2020-12-09 15:49:08','2020-12-09 15:49:08'),(11,'Bases de Dados II','2020-12-09 15:49:08','2020-12-09 15:49:08'),(12,'Engenharia de Software','2020-12-09 15:49:08','2020-12-09 15:49:08'),(13,'Estrutura de Dados e Algoritmos','2020-12-09 15:49:08','2020-12-09 15:49:08'),(14,'Interação Pessoa-computador','2020-12-09 15:49:08','2020-12-09 15:49:08'),(15,'Linguagens de Programação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(16,'Matemática Computacional','2020-12-09 15:49:08','2020-12-09 15:49:08'),(17,'Redes de Computadores I','2020-12-09 15:49:08','2020-12-09 15:49:08'),(18,'Redes de Computadores II','2020-12-09 15:49:08','2020-12-09 15:49:08'),(19,'Sistemas Operativos','2020-12-09 15:49:08','2020-12-09 15:49:08'),(20,'Tecnologias para a Web e Ambientes Móveis','2020-12-09 15:49:08','2020-12-09 15:49:08'),(21,'Administração de Sistemas','2020-12-09 15:49:08','2020-12-09 15:49:08'),(22,'Desenvolvimento de Aplicações Web','2020-12-09 15:49:08','2020-12-09 15:49:08'),(23,'Dinâmica de Grupos e Comunicação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(24,'Estágio ou Projeto Final','2020-12-09 15:49:08','2020-12-09 15:49:08'),(25,'Marketing e Empreendedorismo','2020-12-09 15:49:08','2020-12-09 15:49:08'),(26,'Programação para Dispositivos Móveis','2020-12-09 15:49:08','2020-12-09 15:49:08'),(27,'Projeto Integrado','2020-12-09 15:49:08','2020-12-09 15:49:08'),(28,'Regulação Informática','2020-12-09 15:49:08','2020-12-09 15:49:08'),(29,'Segurança em Redes de Comunicação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(30,'Sistemas de Informação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(31,'Tópicos de Engenharia Informática','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `disciplinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `docentes`
--

LOCK TABLES `docentes` WRITE;
/*!40000 ALTER TABLE `docentes` DISABLE KEYS */;
INSERT INTO `docentes` VALUES (1,25,6,2,'Joao Paulo Barros','Rua Teste 1','7800-001','Beja','910 000 000','test1@ipbeja.pt','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,25,15,2,'Luis Garcias','Rua Teste 2','7800-002','Beja','930 000 000','test2@ipbeja.pt','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,25,22,2,'Luis Bruno','Rua Teste 3','7800-003','Beja','960 000 000','test3@ipbeja.pt','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `docentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `epocas`
--

LOCK TABLES `epocas` WRITE;
/*!40000 ALTER TABLE `epocas` DISABLE KEYS */;
INSERT INTO `epocas` VALUES (1,'normal','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,'recurso','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,'finalistas','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `epocas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `escolas`
--

LOCK TABLES `escolas` WRITE;
/*!40000 ALTER TABLE `escolas` DISABLE KEYS */;
INSERT INTO `escolas` VALUES (1,'Escola Superior Agrária','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,'Escola Superior de Educação','2020-12-09 15:49:08','2020-12-09 15:49:08'),(3,'Escola Superior de Saúde','2020-12-09 15:49:08','2020-12-09 15:49:08'),(4,'Escola Superior Tecnologia e Gestão','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `escolas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2020_12_03_171804_create_escolas_table',1),(7,'2020_12_03_171828_create_epocas_table',1),(8,'2020_12_03_171843_create_disciplinas_table',1),(9,'2020_12_03_171901_create_perfis_table',1),(10,'2020_12_03_171942_create_cursos_table',1),(11,'2020_12_03_172015_create_docentes_table',1),(12,'2020_12_03_172027_create_alunos_table',1),(13,'2020_12_03_172033_create_avaliacoes_table',1),(14,'2020_12_08_111403_create_sessions_table',1),(15,'2020_12_08_115330_alter_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,'Aluno','2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,'Docente','2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('8eekcMoSzOWSw0gHL38NBauQW5o68Y6vZbrV41iK',2,'192.168.1.78','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0','YTo3OntzOjY6Il90b2tlbiI7czo0MDoiblRuVHBOdkZxd2ExeWxKTnpuc240Ym5hSVl3bGhaZ2JqcFAxRGJDOSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI0OiJodHRwOi8vMTkyLjE2OC4xLjcyOjgwMDAiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToyO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkdWI2ZXQzUTdGQnpKbnI4ZUJxSGdyLnhOQjhkLklMaGpzVW9OazVPN3lpN21yMmtjSUduMy4iO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJHViNmV0M1E3RkJ6Sm5yOGVCcUhnci54TkI4ZC5JTGhqc1VvTms1Tzd5aTdtcjJrY0lHbjMuIjt9',1607530491),('hVjVFCrGaJyRRqJGyN3aGrNsveTbonx87yOjAiT5',1,'192.168.1.78','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36','YTo3OntzOjY6Il90b2tlbiI7czo0MDoiZDNSeU1naTY4ZTlRTTFGTUtEOTdDNG9VMzVGYkpKZkJRREk4WEV2YyI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI0OiJodHRwOi8vMTkyLjE2OC4xLjcyOjgwMDAiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkWFJCanFpRC9JSmV4bVBpb2ZuNkQuZUs0RHZqeW1qblZCd0ZTMlpWUWlPWHE0OUJkTElQNWUiO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJFhSQmpxaUQvSUpleG1QaW9mbjZELmVLNER2anltam5WQndGUzJaVlFpT1hxNDlCZExJUDVlIjt9',1607529118),('UTY0wsYt89r6QNvg7mYX7SsznzYgdbgAcjiHbsYw',NULL,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTo0OntzOjY6Il90b2tlbiI7czo0MDoiM0s2dDNKQ1FPU2ttOENRd014NTNLMEVqSUNkS01PZ2MzazdtWG02MiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyMToiaHR0cDovLzEyNy4wLjAuMTo4MDAwIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9sb2dpbiI7fX0=',1607528973);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Miguel Rosa','miguel.rosa17@gmail.com',NULL,'$2y$10$XRBjqiD/IJexmPiofn6D.eK4DvjymjnVBwFS2ZVQiOXq49BdLIP5e',NULL,NULL,NULL,NULL,NULL,'2020-12-09 15:49:08','2020-12-09 15:49:08'),(2,2,'Luis Bruno','lbruno@ipbeja.pt',NULL,'$2y$10$ub6et3Q7FBzJnr8eBqHgr.xNB8d.ILhjsUoNk5O7yi7mr2kcIGn3.',NULL,NULL,NULL,NULL,NULL,'2020-12-09 15:49:08','2020-12-09 15:49:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-09 17:33:54
