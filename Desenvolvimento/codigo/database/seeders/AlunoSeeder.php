<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Aluno;
use Illuminate\Database\Seeder;

class AlunoSeeder extends Seeder
{
    public function run(): void
    {
        $students = [
            [25, 1, 'Joao Rodrigues', 'Rua Teste 1', '7800-001', 'Beja', '910 000 000', 'test1@ipbeja.pt'],
            [25, 1, 'Nuno Batista', 'Rua Teste 2', '7800-002', 'Beja', '930 000 000', 'test2@ipbeja.pt'],
            [25, 1, 'Miguel Rosa', 'Rua Teste 3', '7800-003', 'Beja', '960 000 000', 'miguel.rosa17@gmail.com'],
        ];

        foreach ($students as $student) {
            $aluno = new Aluno;
            $aluno->id_curso = $student[0];
            $aluno->id_perfil = $student[1];
            $aluno->nome = $student[2];
            $aluno->morada = $student[3];
            $aluno->codPostal = $student[4];
            $aluno->localidade = $student[5];
            $aluno->telefone = $student[6];
            $aluno->email = $student[7];
            $aluno->save();
        }
    }
}
