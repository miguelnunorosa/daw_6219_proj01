<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Aluno;
use App\Models\Docente;
use Illuminate\Database\Seeder;

class DocenteSeeder extends Seeder
{
    public function run(): void
    {
        $teachers = [
            [25, 6, 2, 'Joao Paulo Barros', 'Rua Teste 1', '7800-001', 'Beja', '910 000 000', 'test1@ipbeja.pt'],
            [25, 15, 2, 'Luis Garcias', 'Rua Teste 2', '7800-002', 'Beja', '930 000 000', 'test2@ipbeja.pt'],
            [25, 22, 2, 'Luis Bruno', 'Rua Teste 3', '7800-003', 'Beja', '960 000 000', 'test3@ipbeja.pt'],
        ];

        foreach ($teachers as $teacher) {
            $docente = new Docente();
            $docente->id_curso = $teacher[0];
            $docente->id_disciplina = $teacher[1];
            $docente->id_perfil = $teacher[2];
            $docente->nome = $teacher[3];
            $docente->morada = $teacher[4];
            $docente->codPostal = $teacher[5];
            $docente->localidade = $teacher[6];
            $docente->telefone = $teacher[7];
            $docente->email = $teacher[8];
            $docente->save();
        }
    }
}
