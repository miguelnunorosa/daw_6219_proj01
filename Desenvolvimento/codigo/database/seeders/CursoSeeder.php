<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Curso;
use Illuminate\Database\Seeder;

class CursoSeeder extends Seeder
{
    public function run(): void
    {
        $courses_schools = [
            ['CTSP Agropecuária Mediterrânica', 1],
            ['CTSP Análises Laboratoriais', 1],
            ['CTSP Apoio à Infância', 2],
            ['CTSP Apoio em Cuidados Continuados Integrados', 3],
            ['CTSP Comércio Internacional', 4],
            ['CTSP Culturas Regadas', 1],
            ['CTSP Desporto, Lazer e Bem-Estar', 2],
            ['CTSP Eletrónica e Computadores', 4],
            ['CTSP Gestão de Organizações Sociais', 4],
            ['CTSP Informação e Comercialização Turística', 4],
            ['CTSP Inovação e Tecnologia Alimentar', 1],
            ['CTSP Psicogerontologia', 2],
            ['CTSP Redes e Sistemas Informáticos', 4],
            ['CTSP Sistemas de Proteção do Ambiente', 1],
            ['CTSP Som e Imagem', 2],
            ['CTSP Tecnologias Web e Dispositivos Móveis', 4],
            ['CTSP Viticultura e Enologia', 1],
            ['Licenciatura Agronomia', 1],
            ['Licenciatura Audiovisual e Multimédia', 2],
            ['Licenciatura Ciência e Tecnologia dos Alimentos', 1],
            ['Licenciatura Desporto', 2],
            ['Licenciatura Educação Básica', 2],
            ['Licenciatura Enfermagem', 3],
            ['Licenciatura Engenharia do Ambiente', 1],
            ['Licenciatura Engenharia Informática', 4],
            ['Licenciatura Gestão de Empresas', 4],
            ['Licenciatura Gestão de Empresas (Pós-Laboral)', 4],
            ['Licenciatura Serviço Social', 2],
            ['Licenciatura Solicitadoria', 4],
            ['Licenciatura Solicitadoria (Ensino à Distância)', 4],
            ['Licenciatura Tecnologias Bioanalíticas', 1],
            ['Licenciatura Terapia Ocupacional', 2],
            ['Licenciatura Turismo', 4],
            ['Mestrado Agronomia', 1],
            ['Mestrado Atividade Física e Saúde', 2],
            ['Mestrado Contabilidade e Finanças', 4],
            ['Mestrado Desenvolvimento Comunitário e Empreendedorismo', 2],
            ['Mestrado Educação Especial - Especialização no Domínio Cognitivo e Motor', 2],
            ['Mestrado Engenharia de Segurança Informática', 4],
            ['Mestrado Engenharia do Ambiente', 1],
            ['Mestrado Estudos em Enfermagem', 3],
            ['Mestrado Gerontologia Social e Comunitária', 2],
            ['Mestrado Internet das Coisas', 4],
            ['Mestrado Segurança e Higiene no Trabalho', 4],
            ['Mestrado Serviço Social - Riscos Sociais e Desenvolvimento Local', 2],
            ['Pós-Graduação Sistemas de Informação Geográfica no Setor Florestal', 1],
            ['Pós-Graduação Turismo - Gestão e Inovação', 4],
        ];

        foreach ($courses_schools as $course_school) {
            $curso = new Curso;
            $curso->descricao = $course_school[0];
            $curso->id_escola = $course_school[1];
            $curso->save();
        }
    }
}
