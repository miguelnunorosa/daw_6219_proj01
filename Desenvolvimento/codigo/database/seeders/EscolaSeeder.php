<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Escola;
use Illuminate\Database\Seeder;

class EscolaSeeder extends Seeder
{
    public function run()
    {
        $schools = [
            'Escola Superior Agrária',
            'Escola Superior de Educação',
            'Escola Superior de Saúde',
            'Escola Superior Tecnologia e Gestão',
        ];

        foreach ($schools as $school) {
            $escola = new Escola;
            $escola->descricao = $school;
            $escola->save();
        }
    }
}
