<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Curso;
use App\Models\Disciplina;
use Illuminate\Database\Seeder;

class DisciplinaSeeder extends Seeder
{
    public function run(): void
    {
        $lectures = [
            'Álgebra Linear',
            'Análise Matemática',
            'Arquitectura de Computadores',
            'Bases de Dados I',
            'Física Aplicada à Computação',
            'Introdução à Programação I',
            'Introdução à Programação II',
            'Probabilidades e Estatística',
            'Programação Orientada por Objectos',
            'Sistemas Digitais',
            'Bases de Dados II',
            'Engenharia de Software',
            'Estrutura de Dados e Algoritmos',
            'Interação Pessoa-computador',
            'Linguagens de Programação',
            'Matemática Computacional',
            'Redes de Computadores I',
            'Redes de Computadores II',
            'Sistemas Operativos',
            'Tecnologias para a Web e Ambientes Móveis',
            'Administração de Sistemas',
            'Desenvolvimento de Aplicações Web',
            'Dinâmica de Grupos e Comunicação',
            'Estágio ou Projeto Final',
            'Marketing e Empreendedorismo',
            'Programação para Dispositivos Móveis',
            'Projeto Integrado',
            'Regulação Informática',
            'Segurança em Redes de Comunicação',
            'Sistemas de Informação',
            'Tópicos de Engenharia Informática',
        ];

        foreach ($lectures as $lecture) {
            $disciplina = new Disciplina;
            $disciplina->descricao =$lecture;
            $disciplina->save();
        }
    }
}
