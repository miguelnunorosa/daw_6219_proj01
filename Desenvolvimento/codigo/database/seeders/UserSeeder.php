<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        $accounts = [
            [1, 'Miguel Rosa', 'miguel.rosa17@gmail.com', bcrypt('aluno')],
            [2, 'Luis Bruno', 'lbruno@ipbeja.pt', bcrypt('docente')],
        ];

        foreach ($accounts as $account) {
            $user = new User;
            $user->id_perfil = $account[0];
            $user->name = $account[1];
            $user->email = $account[2];
            $user->password = $account[3];
            $user->save();
        }
    }
}
