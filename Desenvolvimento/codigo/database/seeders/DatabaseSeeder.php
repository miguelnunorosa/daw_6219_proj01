<?php declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            EscolaSeeder::class,
            EpocaSeeder::class,
            DisciplinaSeeder::class,
            PerfilSeeder::class,
            CursoSeeder::class,
            DocenteSeeder::class,
            AlunoSeeder::class,
            AvaliacaoSeeder::class,
            UserSeeder::class
        ]);
    }
}
