<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Aluno;
use App\Models\Avaliacao;
use App\Models\Docente;
use Illuminate\Database\Seeder;

class AvaliacaoSeeder extends Seeder
{
    public function run(): void
    {
        $grades = [
            [4, 25, 1, 6, 1, 1, 12, 'sim', ],
            [4, 25, 2, 15, 2, 2, 12, 'sim', ],
            [4, 25, 3, 22, 3, 3, 15, 'sim', ],
        ];

        foreach ($grades as $grade) {
            $avaliacao = new Avaliacao();
            $avaliacao->id_escola = $grade[0];
            $avaliacao->id_curso = $grade[1];
            $avaliacao->id_docente = $grade[2];
            $avaliacao->id_disciplina = $grade[3];
            $avaliacao->id_aluno = $grade[4];
            $avaliacao->id_epoca = $grade[5];
            $avaliacao->classificacao = $grade[6];
            $avaliacao->realizado = $grade[7];
            $avaliacao->save();
        }
    }
}
