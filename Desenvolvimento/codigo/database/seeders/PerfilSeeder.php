<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Perfil;
use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    public function run(): void
    {
        $profiles = [
            'Aluno',
            'Docente'
        ];

        foreach ($profiles as $profile) {
            $perfil = new Perfil();
            $perfil->descricao = $profile;
            $perfil->save();
        }
    }
}
