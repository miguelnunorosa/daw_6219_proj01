<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Epoca;
use Illuminate\Database\Seeder;

class EpocaSeeder extends Seeder
{
    public function run(): void
    {
        $seasons = [
            'normal',
            'recurso',
            'finalistas',
        ];

        foreach ($seasons as $season) {
            $epoca = new Epoca();
            $epoca->descricao = $season;
            $epoca->save();
        }
    }
}
