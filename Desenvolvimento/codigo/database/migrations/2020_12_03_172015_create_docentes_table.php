<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocentesTable extends Migration
{
    public function up(): void
    {
        Schema::create('docentes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_curso');
            $table->unsignedBigInteger('id_disciplina');
            $table->unsignedBigInteger('id_perfil');
            $table->string('nome', 200);
            $table->string('morada', 200);
            $table->string('codPostal', 15);
            $table->string('localidade', 100);
            $table->string('telefone', 20);
            $table->string('email', 200);
            $table->timestamps();

            $table->foreign('id_curso')->references('id')->on('cursos');
            $table->foreign('id_disciplina')->references('id')->on('disciplinas');
            $table->foreign('id_perfil')->references('id')->on('perfis');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('docentes');
    }
}
