<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacoesTable extends Migration
{
    public function up(): void
    {
        Schema::create('avaliacoes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_escola');
            $table->unsignedBigInteger('id_curso');
            $table->unsignedBigInteger('id_docente');
            $table->unsignedBigInteger('id_disciplina');
            $table->unsignedBigInteger('id_aluno');
            $table->unsignedBigInteger('id_epoca');
            $table->float('classificacao');
            $table->char('realizado', 3);
            $table->timestamps();

            $table->foreign('id_escola')->references('id')->on('escolas');
            $table->foreign('id_curso')->references('id')->on('cursos');
            $table->foreign('id_docente')->references('id')->on('docentes');
            $table->foreign('id_disciplina')->references('id')->on('cursos');
            $table->foreign('id_aluno')->references('id')->on('alunos');
            $table->foreign('id_epoca')->references('id')->on('epocas');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('avaliacoes');
    }
}
