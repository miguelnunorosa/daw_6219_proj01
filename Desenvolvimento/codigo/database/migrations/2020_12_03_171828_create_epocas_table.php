<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpocasTable extends Migration
{
    public function up(): void
    {
        Schema::create('epocas', function (Blueprint $table) {
            $table->id();
            $table->string('descricao', 150);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('epocas');
    }
}
