-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: projecto
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alunos` (
  `id_aluno` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(190) NOT NULL,
  `morada` varchar(200) DEFAULT NULL,
  `codPostal` varchar(10) DEFAULT NULL,
  `localidade` varchar(150) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `id_curso` int NOT NULL,
  `id_perfil` int NOT NULL,
  PRIMARY KEY (`id_aluno`),
  KEY `alunos_IdCurso_cursos_IdCurso_fk` (`id_curso`),
  KEY `alunos_idPerfil_alunos_IdPerfil_fk_idx` (`id_perfil`),
  CONSTRAINT `alunos_IdCurso_cursos_IdCurso_fk` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`),
  CONSTRAINT `alunos_idPerfil_alunos_IdPerfil_fk` FOREIGN KEY (`id_perfil`) REFERENCES `perfis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliacoes`
--

DROP TABLE IF EXISTS `avaliacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avaliacoes` (
  `id_avaliacao` int NOT NULL AUTO_INCREMENT,
  `id_escola` int NOT NULL,
  `id_curso` int NOT NULL,
  `id_docente` int NOT NULL,
  `id_disciplina` int NOT NULL,
  `id_aluno` int NOT NULL,
  `id_epoca` int NOT NULL,
  `data` date NOT NULL,
  `realizado` char(3) NOT NULL COMMENT 'sim ou nao',
  PRIMARY KEY (`id_avaliacao`),
  KEY `avaliacoes_IdAluno_alunos_IDAluno_fk` (`id_aluno`),
  KEY `avaliacoes_IdCurso_cursos_IdCurso_fk` (`id_curso`),
  KEY `avaliacoes_IdDocente_docentes_IdDocente_fk` (`id_docente`),
  KEY `avaliacoes_IdEpoca_avaliacoesEpocas_IdEpoca_fk` (`id_epoca`),
  KEY `avaliacoes_IdEscola_scolas_IdEscola_fk` (`id_escola`),
  KEY `avaliacoes_idDisciplina_disciplinas_IdDisciplina_fk` (`id_disciplina`),
  CONSTRAINT `avaliacoes_IdAluno_alunos_IDAluno_fk` FOREIGN KEY (`id_aluno`) REFERENCES `alunos` (`id_aluno`),
  CONSTRAINT `avaliacoes_IdCurso_cursos_IdCurso_fk` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`),
  CONSTRAINT `avaliacoes_idDisciplina_disciplinas_IdDisciplina_fk` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplinas` (`id_disciplina`),
  CONSTRAINT `avaliacoes_IdDocente_docentes_IdDocente_fk` FOREIGN KEY (`id_docente`) REFERENCES `docentes` (`id_docente`),
  CONSTRAINT `avaliacoes_IdEpoca_avaliacoesEpocas_IdEpoca_fk` FOREIGN KEY (`id_epoca`) REFERENCES `avaliacoes_epocas` (`id_epoca`),
  CONSTRAINT `avaliacoes_IdEscola_scolas_IdEscola_fk` FOREIGN KEY (`id_escola`) REFERENCES `escolas` (`id_escola`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacoes`
--

LOCK TABLES `avaliacoes` WRITE;
/*!40000 ALTER TABLE `avaliacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliacoes_epocas`
--

DROP TABLE IF EXISTS `avaliacoes_epocas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avaliacoes_epocas` (
  `id_epoca` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`id_epoca`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacoes_epocas`
--

LOCK TABLES `avaliacoes_epocas` WRITE;
/*!40000 ALTER TABLE `avaliacoes_epocas` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacoes_epocas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cursos` (
  `id_curso` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `id_escola` int NOT NULL,
  PRIMARY KEY (`id_curso`),
  KEY `cursos_idEscola_escolas-IdEscola_fk` (`id_escola`),
  CONSTRAINT `cursos_idEscola_escolas-IdEscola_fk` FOREIGN KEY (`id_escola`) REFERENCES `escolas` (`id_escola`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,'CTSP Agropecuária Mediterrânica',1),(2,'CTSP Análises Laboratoriais',1),(3,'CTSP Apoio à Infância',2),(4,'CTSP Apoio em Cuidados Continuados Integrados',3),(5,'CTSP Comércio Internacional',4),(6,'CTSP Culturas Regadas',1),(7,'CTSP Desporto, Lazer e Bem-Estar',2),(8,'CTSP Eletrónica e Computadores',4),(9,'CTSP Gestão de Organizações Sociais',4),(10,'CTSP Informação e Comercialização Turística',4),(11,'CTSP Inovação e Tecnologia Alimentar',1),(12,'CTSP Psicogerontologia',2),(13,'CTSP Redes e Sistemas Informáticos',4),(14,'CTSP Sistemas de Proteção do Ambiente',1),(15,'CTSP Som e Imagem',2),(16,'CTSP Tecnologias Web e Dispositivos Móveis',4),(17,'CTSP Viticultura e Enologia',1),(18,'Licenciatura Agronomia',1),(19,'Licenciatura Audiovisual e Multimédia',2),(20,'Licenciatura Ciência e Tecnologia dos Alimentos',1),(21,'Licenciatura Desporto',2),(22,'Licenciatura Educação Básica',2),(23,'Licenciatura Enfermagem',3),(24,'Licenciatura Engenharia do Ambiente',1),(25,'Licenciatura Engenharia Informática',4),(26,'Licenciatura Gestão de Empresas',4),(27,'Licenciatura Gestão de Empresas (Pós-Laboral)',4),(28,'Licenciatura Serviço Social',2),(29,'Licenciatura Solicitadoria',4),(30,'Licenciatura Solicitadoria (Ensino à Distância)',4),(31,'Licenciatura Tecnologias Bioanalíticas',1),(32,'Licenciatura Terapia Ocupacional',2),(33,'Licenciatura Turismo',4),(48,'Mestrado Agronomia',1),(49,'Mestrado Atividade Física e Saúde',2),(50,'Mestrado Contabilidade e Finanças',4),(51,'Mestrado Desenvolvimento Comunitário e Empreendedorismo',2),(52,'Mestrado Educação Especial - Especialização no Domínio Cognitivo e Motor',2),(53,'Mestrado Engenharia de Segurança Informática',4),(54,'Mestrado Engenharia do Ambiente',1),(55,'Mestrado Estudos em Enfermagem',3),(56,'Mestrado Gerontologia Social e Comunitária',2),(57,'Mestrado Internet das Coisas',4),(58,'Mestrado Segurança e Higiene no Trabalho',4),(59,'Mestrado Serviço Social - Riscos Sociais e Desenvolvimento Local',2),(60,'Pós-Graduação Sistemas de Informação Geográfica no Setor Florestal ',1),(61,'Pós-Graduação Turismo - Gestão e Inovação',4);
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplinas`
--

DROP TABLE IF EXISTS `disciplinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `disciplinas` (
  `id_disciplina` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(150) NOT NULL,
  PRIMARY KEY (`id_disciplina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplinas`
--

LOCK TABLES `disciplinas` WRITE;
/*!40000 ALTER TABLE `disciplinas` DISABLE KEYS */;
/*!40000 ALTER TABLE `disciplinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docentes`
--

DROP TABLE IF EXISTS `docentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `docentes` (
  `id_docente` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `morada` varchar(200) DEFAULT NULL,
  `codPostal` varchar(15) DEFAULT NULL,
  `localidade` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `id_curso` int DEFAULT NULL,
  `id_disciplina` int DEFAULT NULL,
  `id_perfil` int NOT NULL,
  PRIMARY KEY (`id_docente`),
  KEY `docentes_IdCursos_cursos_IdCurso_fk` (`id_curso`),
  KEY `docentes_IdDisciplina_disciplinas_IdDisciplinas_fk` (`id_disciplina`),
  KEY `docentes_IdPerfil_perfis_IdPerfil_fk_idx` (`id_perfil`),
  CONSTRAINT `docentes_IdCursos_cursos_IdCurso_fk` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`),
  CONSTRAINT `docentes_IdDisciplina_disciplinas_IdDisciplinas_fk` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplinas` (`id_disciplina`),
  CONSTRAINT `docentes_IdPerfil_perfis_IdPerfil_fk` FOREIGN KEY (`id_perfil`) REFERENCES `perfis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docentes`
--

LOCK TABLES `docentes` WRITE;
/*!40000 ALTER TABLE `docentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `docentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escolas`
--

DROP TABLE IF EXISTS `escolas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escolas` (
  `id_escola` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(150) NOT NULL,
  PRIMARY KEY (`id_escola`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escolas`
--

LOCK TABLES `escolas` WRITE;
/*!40000 ALTER TABLE `escolas` DISABLE KEYS */;
INSERT INTO `escolas` VALUES (1,'Escola Superior Agrária'),(2,'Escola Superior de Educação'),(3,'Escola Superior de Saúde'),(4,'Escola Superior Tecnologia e Gestão');
/*!40000 ALTER TABLE `escolas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perfis` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,'Aluno'),(2,'Docente');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-18 12:27:28
