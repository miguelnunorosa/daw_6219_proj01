<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    use HasFactory;

    protected $table = 'docentes';

    protected $fillable = [
        'id_curso',
        'id_disciplina',
        'id_perfil',
        'nome',
        'morada',
        'codPostal',
        'localidade',
        'telefone',
        'email',
    ];
}
