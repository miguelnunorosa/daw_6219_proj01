<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    use HasFactory;

    protected $table = 'avaliacoes';

    protected $fillable = [
        'id_escola',
        'id_curso',
        'id_docente',
        'id_disciplina',
        'id_aluno',
        'id_epoca',
        'realizado',
    ];

    public function scopeStudentList(Builder $query): Collection
    {
        $fields = [
            $this->table . '.id AS id',
            'escolas.descricao AS escola',
            'cursos.descricao AS curso',
            'docentes.nome AS docente',
            'disciplinas.descricao AS disciplina',
            'alunos.nome AS aluno',
            'epocas.descricao AS epoca',
            $this->table . '.classificacao AS classificacao',
            $this->table . '.realizado AS realizado',
        ];

        return $query
            ->select(...$fields)
            ->join('escolas', $this->table . '.id_escola', '=', 'escolas.id')
            ->join('cursos', $this->table . '.id_curso', '=', 'cursos.id')
            ->join('docentes', $this->table . '.id_docente', '=', 'docentes.id')
            ->join('disciplinas', $this->table . '.id_disciplina', '=', 'disciplinas.id')
            ->join('alunos', $this->table . '.id_aluno', '=', 'alunos.id')
            ->join('epocas', $this->table . '.id_epoca', '=', 'epocas.id')
            ->get();
    }

    public function scopeTeacherList(Builder $query, int $teacher_id): Collection
    {
        $fields = [
            $this->table . '.id AS id',
            'escolas.descricao AS escola',
            'cursos.descricao AS curso',
            'disciplinas.descricao AS disciplina',
            'alunos.nome AS aluno',
            'epocas.descricao AS epoca',
            $this->table . '.classificacao AS classificacao',
            $this->table . '.realizado AS realizado',
        ];

        return $query
            ->select(...$fields)
            ->join('escolas', $this->table . '.id_escola', '=', 'escolas.id')
            ->join('cursos', $this->table . '.id_curso', '=', 'cursos.id')
            ->join('disciplinas', $this->table . '.id_disciplina', '=', 'disciplinas.id')
            ->join('alunos', $this->table . '.id_aluno', '=', 'alunos.id')
            ->join('epocas', $this->table . '.id_epoca', '=', 'epocas.id')
            ->where($this->table . '.id_docente', '=', $teacher_id)
            ->get();
    }
}
