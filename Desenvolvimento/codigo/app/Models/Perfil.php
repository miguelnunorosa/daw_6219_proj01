<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Perfil extends Model
{
    use HasFactory;

    const DOCENTE = 'Docente';

    protected $table = 'perfis';

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'id_perfil', 'id');
    }
}
