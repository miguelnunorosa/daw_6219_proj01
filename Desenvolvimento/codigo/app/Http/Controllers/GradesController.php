<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Avaliacao;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GradesController extends Controller
{
    const ID_DOCENTE = 3;

    public function index(): View
    {
        $avaliacoes = Auth::user()->isTeacher()
            ? Avaliacao::teacherList(self::ID_DOCENTE) /*caso o isTeacher retorne 1 */
            : Avaliacao::studentList();

        $view = Auth::user()->isTeacher()
            ? 'baseStruct.avaliacao.docente.index'
            : 'baseStruct.avaliacao.aluno.index';

        return view($view, compact('avaliacoes'));
    }

    public function create(): View
    {
        return view('baseStruct.avaliacao.docente.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $payload = $request->post();

        $avaliacao = new Avaliacao;
        $avaliacao->id_escola = $payload['id_escola'];
        $avaliacao->id_curso = $payload['id_curso'];
        $avaliacao->id_disciplina = $payload['id_disciplina'];
        $avaliacao->id_aluno = $payload['id_aluno'];
        $avaliacao->id_epoca = $payload['id_epoca'];
        $avaliacao->id_docente = self::ID_DOCENTE;
        $avaliacao->classificacao = $payload['classificacao'];
        $avaliacao->realizado = $payload['classificacao'] < 10 ? 'nao' : 'sim';
        $avaliacao->save();

        return redirect('/');
    }
}
