<?php
/** @var array $avaliacoes */
?>
@include('baseStruct.head.index')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Avaliações') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <main role="main" style="margin: 25px 25px 25px 25px;">
                    <button type="button" class="btn btn-primary" onclick="window.location='{{ route('grades.create') }}'">Adicionar Classificação</button>

                    <br />
                    <br />

                    <div class="main-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Escola</th>
                                <th scope="col">Curso</th>
                                <th scope="col">Disciplina</th>
                                <th scope="col">Aluno</th>
                                <th scope="col">Epoca</th>
                                <th scope="col">Classif.</th>
                                <th scope="col">Realizado</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($avaliacoes as $avaliacao)
                                <tr>
                                    <th scope="row">{{$avaliacao->id}}</th>
                                    <td>{{$avaliacao->escola}}</td>
                                    <td>{{$avaliacao->curso}}</td>
                                    <td>{{$avaliacao->disciplina}}</td>
                                    <td>{{$avaliacao->aluno}}</td>
                                    <td>{{$avaliacao->epoca}}</td>
                                    <td>{{$avaliacao->classificacao}}</td>
                                    <td>{{$avaliacao->realizado}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </main>
            </div>
        </div>
    </div>
</x-app-layout>
