@include('baseStruct.head.index')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <x-jet-nav-link href="{{ route('grades.index') }}" :active="request()->routeIs('grades.index')">
                {{ __('Avaliações') }}
            </x-jet-nav-link> / {{__('Adicionar')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <main role="main" class="container" style="margin-top: 25px;">
                    <div class="main-content">
                        <form method="POST" action="{{route('grades.store')}}">
                            @csrf

                            <div class="form-group">
                                <label for="escolas">Escolas</label>
                                <select class="form-control" id="escolas" name="id_escola">
                                    <option></option>
                                    @foreach(\App\Models\Escola::all() as $escola)
                                        <option value="{{$escola->id}}">{{$escola->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="cursos">Cursos</label>
                                <select class="form-control" id="cursos" name="id_curso" disabled>
                                    <option></option>
                                    @foreach(\App\Models\Curso::all() as $curso)
                                        <option value="{{$curso->id}}">{{$curso->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="disciplinas">Disciplinas</label>
                                <select class="form-control" id="disciplinas" name="id_disciplina" disabled>
                                    <option></option>
                                    @foreach(\App\Models\Disciplina::all() as $disciplina)
                                        <option value="{{$disciplina->id}}">{{$disciplina->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="alunos">Alunos</label>
                                <select class="form-control" id="alunos" name="id_aluno" disabled>
                                    <option></option>
                                    @foreach(\App\Models\Aluno::all() as $aluno)
                                        <option value="{{$aluno->id}}">{{$aluno->nome}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="epocas">Epocas</label>
                                <select class="form-control" id="epocas" name="id_epoca" disabled>
                                    <option></option>
                                    @foreach(\App\Models\Epoca::all() as $epoca)
                                        <option value="{{$epoca->id}}">{{$epoca->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="classificacao">Classificação *</label>
                                <input class="form-control" id="classificacao" name="classificacao" type="text" required disabled>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" id="guardar" disabled>Guardar</button>
                            </div>
                        </form>
                    </div>

                </main>

            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(function() {
        $('#escolas').on('change', function() {
            if ($(this).val() !== '') {
                $('#cursos').removeAttr('disabled');
            }
            else {
                $('#cursos').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#disciplinas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#alunos').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#epocas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#classificacao').val('').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });

    $(function() {
        $('#cursos').on('change', function() {
            if ($(this).val() !== '') {
                $('#disciplinas').val('').removeAttr('disabled');
            }
            else {
                $('#disciplinas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#alunos').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#epocas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#classificacao').val('').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });

    $(function() {
        $('#disciplinas').on('change', function() {
            if ($(this).val() !== '') {
                $('#alunos').removeAttr('disabled');
            }
            else {
                $('#alunos').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#epocas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#classificacao').val('').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });

    $(function() {
        $('#alunos').on('change', function() {
            if ($(this).val() !== '') {
                $('#epocas').removeAttr('disabled');
            }
            else {
                $('#epocas').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#classificacao').val('').prop("selectedIndex", 0).attr('disabled', 'disabled');
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });

    $(function() {
        $('#epocas').on('change', function() {
            if ($(this).val() !== '') {
                $('#classificacao').removeAttr('disabled');
            }
            else {
                $('#classificacao').val('').attr('disabled', 'disabled');
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });

    $(function() {
        $('#classificacao').on('change', function() {
            if ($(this).val() !== '') {
                $('#guardar').removeAttr('disabled');
            }
            else {
                $('#guardar').attr('disabled', 'disabled');
            }
        });
    });
</script>
